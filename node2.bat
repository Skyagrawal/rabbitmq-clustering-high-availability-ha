set RABBITMQ_NODE_PORT=6002
set RABBITMQ_NODENAME=rabbit2
set RABBITMQ_SERVICE_NAME=rabbit2
set RABBITMQ_SERVER_START_ARGS=-rabbitmq_management listener [{port,15673}]
call rabbitmq-server -detached