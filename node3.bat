set RABBITMQ_NODE_PORT=6003
set RABBITMQ_NODENAME=rabbit3
set RABBITMQ_SERVICE_NAME=rabbit3
set RABBITMQ_SERVER_START_ARGS=-rabbitmq_management listener [{port,15674}]
call rabbitmq-server -detached