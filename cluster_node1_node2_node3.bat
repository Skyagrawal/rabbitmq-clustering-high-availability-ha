call rabbitmqctl -n rabbit2 stop_app
call rabbitmqctl -n rabbit2 join_cluster rabbit1@<hostname>
call rabbitmqctl -n rabbit2 start_app
call rabbitmqctl -n rabbit3 stop_app
call rabbitmqctl -n rabbit3 join_cluster rabbit1@<hostname>
call rabbitmqctl -n rabbit3 start_app